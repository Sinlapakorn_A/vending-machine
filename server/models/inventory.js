'use strict'
const { Model } = require('sequelize')
module.exports = (sequelize, DataTypes) => {
  class Inventory extends Model {
    /**
     * Helper method for defining associations.
     * This method is not a part of Sequelize lifecycle.
     * The `models/index` file will call this method automatically.
     */
    static associate(_models) {
      // define association here
    }
  }
  Inventory.init(
    {
      title: DataTypes.STRING,
      icon: DataTypes.STRING,
      alt: DataTypes.STRING,
      type: DataTypes.STRING,
      score: DataTypes.INTEGER,
      size: DataTypes.STRING,
      price: DataTypes.STRING,
      quantity: DataTypes.INTEGER,
    },
    {
      sequelize,
      modelName: 'Inventory',
    }
  )
  return Inventory
}
