# Generate Model
npx sequelize-cli model:generate --name Inventory --attributes title:string,icon:string,alt:string,type:string,score:integer,size:string,price:string,quantity: integer

# Generate Seed
npx sequelize-cli seed:generate --name demo-inventory

# Migrate
npx sequelize-cli db:migrate

# Seed
npx sequelize-cli db:seed:all