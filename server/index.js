const express = require('express')
const consola = require('consola')
const { Nuxt, Builder } = require('nuxt')
const app = express()

const config = require('../nuxt.config.js')
const categoryController = require('./controllers/category')
const inventoyController = require('./controllers/inventory')

// Import and Set Nuxt.js options
config.dev = process.env.NODE_ENV !== 'production'

async function start() {
  // Init Nuxt.js
  const nuxt = new Nuxt(config)

  const { host, port } = nuxt.options.server

  await nuxt.ready()
  // Build only in dev mode
  if (config.dev) {
    const builder = new Builder(nuxt)
    await builder.build()
  }
  // JSON Parser
  app.use(express.json())

  // Inventory Controller routing
  app.get('/api/category', categoryController.getCategory)
  app.get('/api/inventory', inventoyController.getInventories)
  app.post('/api/inventory', inventoyController.restoreInventories)
  app.put('/api/inventory', inventoyController.updateInventories)

  // Give nuxt middleware to express
  app.use(nuxt.render)

  // Listen the server
  app.listen(port, host)
  consola.ready({
    message: `Server listening on http://${host}:${port}`,
    badge: true,
  })
}
start()
