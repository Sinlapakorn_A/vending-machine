const nodemailer = require('nodemailer')
const _ = require('lodash')
const db = require('../models')

module.exports = {
  getInventories: async (_req, res) => {
    try {
      const inventories = await db.Inventory.findAll()
      return res.json(inventories)
    } catch (e) {
      return res.status(500).json({ message: 'Cannot get data from database.' })
    }
  },
  updateInventories: async (req, res) => {
    const data = req.body
    if (data) {
      try {
        // Update inventories
        let caseUpdate = ''
        let whereId = ''
        data.forEach((item) => {
          caseUpdate += ` when id = ${item.id} then ${
            item.quantity - item.amount
          }`
          whereId += `${item.id},`
        })
        whereId = whereId.slice(0, -1)
        const updateInventories = await db.sequelize.query(`
          UPDATE Inventories
          SET quantity = (case${caseUpdate} end)
            WHERE id in(${whereId});
        `)

        // Check inventories
        let inventories = await db.Inventory.findAll()
        inventories = JSON.parse(JSON.stringify(inventories, null, 2))
        inventories = _.filter(inventories, (inventory) => {
          return inventory.quantity < 10
        })
        if (!_.isEmpty(inventories)) {
          // Send Email
          const transporter = nodemailer.createTransport({
            host: 'smtp.gmail.com',
            port: 587,
            secure: false,
            requireTLS: true,
            auth: {
              user: process.env.GMAIL_USER,
              pass: process.env.GMAIL_PASSWORD,
            },
          })
          const mailOptions = {
            from: process.env.GMAIL_USER,
            to: process.env.GMAIL_SEND_TO,
            subject: 'Quantity item is less then 10',
            text: `${inventories}`,
          }
          transporter.sendMail(mailOptions, (error, _info) => {
            if (error) {
              return console.log('send email - error >>>> ', error.message)
            }
            console.log('send email - success')
          })
        }

        // return status
        return res.status(200).json(updateInventories)
      } catch (e) {
        console.log('Message e: %s', e)
        return res
          .status(500)
          .json({ message: 'Cannot update data to database.' })
      }
    }
    return res.status(400).json({ message: 'Bad request.' })
  },
  restoreInventories: async (_req, res) => {
    try {
      const restoreInventories = await db.Inventory.update(
        { quantity: 20 },
        { where: {} }
      )
      return res.status(205).json(restoreInventories)
    } catch (e) {
      console.log('e', e)
      return res
        .status(500)
        .json({ message: 'Cannot restore data to database.' })
    }
  },
}
