module.exports = {
  getCategory: (_req, res) => {
    try {
      const category = [
        {
          title: 'coffee',
          icon: '/coffee.png',
          alt: 'coffee-icon',
        },
        {
          title: 'milk',
          icon: '/milk-box.png',
          alt: 'milk-box-icon',
        },
        {
          title: 'juice',
          icon: '/juice.png',
          alt: 'juice-icon',
        },
      ]
      return res.json(category)
    } catch (e) {
      return res.status(500).json({ message: 'Cannot get data from database.' })
    }
  },
}
