export default {
  async getInventories({ commit }) {
    await this.$axios.$get('/api/inventory').then((res) => {
      commit('setInventories', res)
    })
  },
  async updateInventories({ dispatch }, data) {
    await this.$axios.$put('/api/inventory', data)
    await dispatch('getInventories')
  },
  async restoreInventories(_) {
    await this.$axios.$post('/api/inventory')
  },
}
