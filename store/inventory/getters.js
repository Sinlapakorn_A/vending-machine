const _ = require('lodash')

export default {
  menuList: (state) => {
    return _.groupBy(state.inventories, 'type')
  },
  itemInCart: (state) => {
    return _.chain(state.inventories)
      .filter((inventory) => {
        return _.includes(_.keys(state.cart), String(inventory.id))
      })
      .reduce((result, value) => {
        result.push({
          ...value,
          amount: state.cart[value.id],
        })
        return result
      }, [])
      .value()
  },
  sumItemInCart: (state) => {
    return _.chain(state.cart).values().sum().value()
  },
}
