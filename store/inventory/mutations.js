const _ = require('lodash')

export default {
  setInventories(state, payload) {
    state.inventories = payload
  },
  addCart(state, payload) {
    state.cart[payload] = (state.cart[payload] || 0) + 1
    updateCart(state)
  },
  clearCart(state) {
    state.cart = {}
  },
  increaseItem(state, payload) {
    state.cart[payload] = state.cart[payload] + 1
    updateCart(state)
  },
  decreaseItem(state, payload) {
    state.cart[payload] = state.cart[payload] - 1
    updateCart(state)
  },
}

function updateCart(state) {
  const newCart = _.pickBy(state.cart, _.identity)
  state.cart = _.cloneDeep(newCart)
}
