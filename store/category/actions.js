export default {
  async getCategory({ commit }) {
    await this.$axios.$get('/api/category').then((res) => {
      commit('setCategory', res)
    })
  }
}