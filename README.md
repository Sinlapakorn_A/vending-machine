<!-- GETTING STARTED -->
## Getting Started
This is an example of how you may give instructions on setting up your project locally.
To get a local copy up and running follow these simple example steps.

### Demo
   Unavailable


### Installation
1. Clone the repo
   ```sh
   git clone https://gitlab.com/Sinlapakorn_A/vending-machine
   ```
2. Install NPM packages
   ```sh
   yarn
   ```
2. Create .env file in root folder
   ```sh
   GMAIL_USER= your email (GMAIL)
   GMAIL_PASSWORD= the password for an email
   GMAIL_SEND_TO= emails that you need to be sent
   ```

### Run
1. Start this application
   ```sh
   yarn dev
   ```
2. Then open [http://localhost:3000/](http://localhost:3000/) to see your app.
